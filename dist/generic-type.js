"use strict";
class Queue {
    constructor() {
        this.data = [];
    }
    push(item) {
        this.data.push(item);
    }
    pop() {
        this.data.shift();
    }
    getAll() {
        return this.data;
    }
}
const people = new Queue();
people.push({
    id: 13,
    name: "Milk",
    code: "gg",
    method1: () => console.log(""),
    method2: () => 2
});
// people.push("abc"); //forbidden because of constraint
