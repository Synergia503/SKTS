"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const american_pizzeria_1 = require("./american-pizzeria");
const pizza_model_1 = require("./pizza.model");
const polish_pizzeria_1 = require("./polish-pizzeria");
const laStrada = new polish_pizzeria_1.PolishPizzeria("La Strada", true);
const americanHouse = new american_pizzeria_1.AmericanPizzeria("AmericanHouse");
const polishPizzeria = new polish_pizzeria_1.PolishPizzeria("PolishPizzeria", true);
const capricciosa = {
    name: "capricciosa",
    price: 12.99,
    size: "large",
    status: pizza_model_1.Status.Ordered,
    cancelable: false
};
polishPizzeria.order(capricciosa);
americanHouse.order(capricciosa);
polishPizzeria.changeStatus(0, pizza_model_1.Status.Baked);
polishPizzeria.changeSize(0, "large");
console.log(polishPizzeria);
