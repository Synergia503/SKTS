"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const countries_1 = require("./countries");
class CountriesInformation {
    constructor() {
        this.countriesByArea = [];
        this.countriesFromEurope = this.filterCountriesFromEurope();
        this.countriesWhereCapitalStartsWithF = countries_1.countries.filter(this.filterCapitalByLetter("F"));
        this.countriesWithExtendedInfo = this.getCountriesExtendedInfo();
        this.countriesWithShortInfo = this.getCountriesShortInfo();
        this.totalPopulation = this.getTotalPopulation();
        this.polishNeighborsPopulation = this.getPolishNeighborsPopulation();
        this.filterCountriesByArea();
    }
    filterCountriesByArea() {
        this.countriesByArea = countries_1.countries.filter(({ area }) => area < 30000 || area > 100000);
    }
    filterCountriesFromEurope() {
        return countries_1.countries.filter(({ region }) => region === "Europe");
    }
    filterCapitalByLetter(letter) {
        return (country) => country.capital.startsWith(letter);
    }
    getCountriesExtendedInfo() {
        return countries_1.countries.map(({ name, borders, timezones }) => {
            return {
                name: name,
                neighbours: borders.length > 0 ? borders.join(" ") : "HOME",
                timezoneInfo: ` Timezone:  ${timezones.join()}`
            };
        });
    }
    getCountriesShortInfo() {
        return countries_1.countries.map(({ name, area, population }) => `${name} has area: ${area} km2 and population amounts to: ${population}`);
    }
    getTotalPopulation() {
        return countries_1.countries.reduce((acc, next) => acc + next.population, 0);
    }
    getPolishNeighborsPopulation() {
        return countries_1.countries
            .filter(({ borders }) => borders.includes("POL"))
            .reduce((acc, next) => acc + next.population, 0);
    }
}
exports.CountriesInformation = CountriesInformation;
const countriesInformation = new CountriesInformation();
