"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const countries_1 = require("./countries");
const filterCountriesByArea = () => countries_1.countries.filter(({ area }) => area < 30000 || area > 100000);
const filterCountriesFromEurope = () => countries_1.countries.filter(({ region }) => region === "Europe");
const filterCapitalByLetter = (letter) => (country) => country.capital.startsWith(letter);
const getCountriesExtendedInfo = () => {
    return countries_1.countries.map(({ name, borders, timezones }) => {
        return {
            name: name,
            neighbours: borders.length > 0 ? borders.join(" ") : "HOME",
            timezoneInfo: ` Timezone:  ${timezones.join()}`
        };
    });
};
const getCountriesShortInfo = () => countries_1.countries.map(({ name, area, population }) => `${name} has area: ${area} km2 and population amounts to: ${population}`);
const getTotalPopulation = () => countries_1.countries.reduce((acc, next) => acc + next.population, 0);
const getPolishNeighborsPopulation = () => {
    return countries_1.countries
        .filter(({ borders }) => borders.includes("POL"))
        .reduce((acc, next) => acc + next.population, 0);
};
const countriesByArea = filterCountriesByArea();
const countriesFromEurope = filterCountriesFromEurope();
const countriesWhereCapitalStartsWithF = countries_1.countries.filter(filterCapitalByLetter("F"));
const countriesWithExtendedInfo = getCountriesExtendedInfo();
const countriesWithShortInfo = getCountriesShortInfo();
const totalPopulation = getTotalPopulation();
const polishNeighborsPopulation = getPolishNeighborsPopulation();
