"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Status;
(function (Status) {
    Status[Status["Ordered"] = 0] = "Ordered";
    Status[Status["Baked"] = 1] = "Baked";
    Status[Status["Sold"] = 2] = "Sold"; // 2
})(Status = exports.Status || (exports.Status = {}));
var PizzaSize;
(function (PizzaSize) {
    PizzaSize["extraSmall"] = "xs";
    PizzaSize["small"] = "small";
    PizzaSize["medium"] = "medium";
    PizzaSize["large"] = "large";
})(PizzaSize || (PizzaSize = {}));
