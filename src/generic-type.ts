interface Item {
  name: string;
  method1(): void;
}

interface Item {
  code: string;
  method2(): number;
}

interface ProductsQueue<T> {
  push(item: T): void;
  getAll(): T[];
}

class Queue<T extends Item> implements ProductsQueue<T> {
  private data: T[] = [];

  push(item: T) {
    this.data.push(item);
  }

  pop() {
    this.data.shift();
  }

  getAll() {
    return this.data;
  }
}

interface ProductItem extends Item {
  id: number;
}

const people = new Queue<ProductItem & Item>();
people.push({
  id: 13,
  name: "Milk",
  code: "gg",
  method1: () => console.log(""),
  method2: () => 2
});
// people.push("abc"); //forbidden because of constraint
