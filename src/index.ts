import { AmericanPizzeria } from "./american-pizzeria";
import { Pizza, Status } from "./pizza.model";
import { Pizzeria } from "./pizzeria";
import { PolishPizzeria } from "./polish-pizzeria";

const laStrada = new PolishPizzeria("La Strada", true);
const americanHouse = new AmericanPizzeria("AmericanHouse");
const polishPizzeria = new PolishPizzeria("PolishPizzeria", true);

const capricciosa = {
  name: "capricciosa",
  price: 12.99,
  size: "large",
  status: Status.Ordered,
  cancelable: false
};

const prosciutto = {
  name: "prosciutto",
  price: 22.99,
  size: "small",
  status: Status.Sold,
  cancelable: true
};

polishPizzeria.order(capricciosa);
polishPizzeria.order(prosciutto);
americanHouse.order(capricciosa);
polishPizzeria.changeStatus(0, Status.Baked);
polishPizzeria.changeSize(0, "large");

const table = document.getElementById("table");

const createTrNode = (pizza: Pizza) => {
  return `<tr>
            <td>${pizza.name}</td>
            <td>${pizza.price}</td>
            <td>${pizza.size}</td>
          </tr>`;
};

const getRowsWithOrderedPizzas = (polishPizzeria: PolishPizzeria) => {
  return polishPizzeria.pizzasInOrder.map(pizza => createTrNode(pizza)).join();
};

table.innerHTML = getRowsWithOrderedPizzas(polishPizzeria);
