import { AmericanPizzeria } from "./american-pizzeria";
import { Status } from "./pizza.model";
import { PolishPizzeria } from "./polish-pizzeria";

const laStrada = new PolishPizzeria("La Strada", true);
const americanHouse = new AmericanPizzeria("AmericanHouse");
const polishPizzeria = new PolishPizzeria("PolishPizzeria", true);

const capricciosa = {
  name: "capricciosa",
  price: 12.99,
  size: "large",
  status: Status.Ordered,
  cancelable: false
};

polishPizzeria.order(capricciosa);
americanHouse.order(capricciosa);
polishPizzeria.changeStatus(0, Status.Baked);
polishPizzeria.changeSize(0, "large");
console.log(polishPizzeria);
