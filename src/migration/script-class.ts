import { countries } from "./countries";
import { Country, CountryExtendedInfo } from "./country";

export class CountriesInformation {
  countriesByArea: Country[] = [];
  countriesFromEurope: Country[] = this.filterCountriesFromEurope();
  countriesWhereCapitalStartsWithF: Country[] = countries.filter(
    this.filterCapitalByLetter("F")
  );

  countriesWithExtendedInfo = this.getCountriesExtendedInfo();
  countriesWithShortInfo = this.getCountriesShortInfo();
  totalPopulation = this.getTotalPopulation();
  polishNeighborsPopulation = this.getPolishNeighborsPopulation();

  constructor() {
    this.filterCountriesByArea();
  }

  filterCountriesByArea(): void {
    this.countriesByArea = countries.filter(
      ({ area }) => area < 30000 || area > 100000
    );
  }

  filterCountriesFromEurope(): Country[] {
    return countries.filter(({ region }) => region === "Europe");
  }

  filterCapitalByLetter(letter: string) {
    return (country: Country): boolean => country.capital.startsWith(letter);
  }

  getCountriesExtendedInfo(): CountryExtendedInfo[] {
    return countries.map(({ name, borders, timezones }) => {
      return {
        name: name,
        neighbours: borders.length > 0 ? borders.join(" ") : "HOME",
        timezoneInfo: ` Timezone:  ${timezones.join()}`
      };
    });
  }

  getCountriesShortInfo(): string[] {
    return countries.map(
      ({ name, area, population }) =>
        `${name} has area: ${area} km2 and population amounts to: ${population}`
    );
  }

  getTotalPopulation(): number {
    return countries.reduce((acc, next) => acc + next.population, 0);
  }

  getPolishNeighborsPopulation(): number {
    return countries
      .filter(({ borders }) => borders.includes("POL"))
      .reduce((acc, next) => acc + next.population, 0);
  }
}

const countriesInformation = new CountriesInformation();
