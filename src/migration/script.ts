import { countries } from "./countries";
import { Country, CountryExtendedInfo } from "./country";

const filterCountriesByArea = (): Country[] =>
  countries.filter(({ area }) => area < 30000 || area > 100000);

const filterCountriesFromEurope = (): Country[] =>
  countries.filter(({ region }) => region === "Europe");

const filterCapitalByLetter = (letter: string) => (country: Country): boolean =>
  country.capital.startsWith(letter);

const getCountriesExtendedInfo = (): CountryExtendedInfo[] => {
  return countries.map(({ name, borders, timezones }) => {
    return {
      name: name,
      neighbours: borders.length > 0 ? borders.join(" ") : "HOME",
      timezoneInfo: ` Timezone:  ${timezones.join()}`
    };
  });
};

const getCountriesShortInfo = (): string[] =>
  countries.map(
    ({ name, area, population }) =>
      `${name} has area: ${area} km2 and population amounts to: ${population}`
  );

const getTotalPopulation = (): number =>
  countries.reduce((acc, next) => acc + next.population, 0);

const getPolishNeighborsPopulation = (): number => {
  return countries
    .filter(({ borders }) => borders.includes("POL"))
    .reduce((acc, next) => acc + next.population, 0);
};

const countriesByArea = filterCountriesByArea();
const countriesFromEurope = filterCountriesFromEurope();
const countriesWhereCapitalStartsWithF: Country[] = countries.filter(
  filterCapitalByLetter("F")
);
const countriesWithExtendedInfo = getCountriesExtendedInfo();
const countriesWithShortInfo = getCountriesShortInfo();
const totalPopulation = getTotalPopulation();
const polishNeighborsPopulation = getPolishNeighborsPopulation();
