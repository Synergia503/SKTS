import { DebitCard } from "./pizza.model";
export interface Pizza {
  name: string;
  price: number | string;
  size: string;
  status: Status;
}

export enum Status {
  Ordered, // 0
  Baked, // 1
  Sold // 2
}

enum PizzaSize {
  extraSmall = "xs",
  small = "small",
  medium = "medium",
  large = "large"
}

export type SizeKey = keyof typeof PizzaSize;

export interface Cash {
  type: "cash";
  currency: string;
}

export interface DebitCard {
  type: "debitCard";
  code: string;
}

export interface OnlinePayment {
  type: "onlinePayment";
  bankAccount: number;
}

export type PaymentMethod = Cash | DebitCard | OnlinePayment;
