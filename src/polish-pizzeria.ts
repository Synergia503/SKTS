import { Pizza } from "./pizza.model";
import { Pizzeria } from "./pizzeria";

interface CancelablePizza {
  cancelable: boolean;
}

function PizzaCreated(target: Function) {
  console.log("Pizza creation");
}

function ClosedAtNight<T extends { new (...args: any[]): {} }>(constructor: T) {
  return class extends constructor {
    openAtNight = false;
  };
}

function Enumerable(value: boolean) {
  return function(
    target: any,
    propertyKey: string,
    propertyDescriptor: PropertyDescriptor
  ) {
    propertyDescriptor.enumerable = value;
  };
}

@ClosedAtNight
@PizzaCreated
export class PolishPizzeria extends Pizzeria {
  constructor(name: string, public openAtNight: boolean) {
    // console.log(); cannot be above super
    super(name);
    console.log();
  }

  order(pizza: Pizza & CancelablePizza) {
    this.pizzasInOrder.push(pizza);
  }

  @Enumerable(false)
  bake() {
    return "Pizza is being baked";
  }
}
